/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements an object type that can store twelve corresponding
 * values within one object.
 * @param <A> the type of the first argument of the duodecuplet
 * @param <B> the type of the second argument of the duodecuplet
 * @param <C> the type of the third argument of the duodecuplet
 * @param <D> the type of the fourth argument of the duodecuplet
 * @param <E> the type of the fifth argument of the duodecuplet
 * @param <F> the type of the sixth argument of the duodecuplet
 * @param <G> the type of the seventh argument of the duodecuplet
 * @param <H> the type of the eighth argument of the duodecuplet
 * @param <I> the type of the ninth argument of the duodecuplet
 * @param <J> the type of the tenth argument of the duodecuplet
 * @param <K> the type of the eleventh argument of the duodecuplet
 * @param <L> the type of the twelfth argument of the duodecuplet
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public final class Duodecuplet<A, B, C, D, E, F, G, H, I, J, K, L> extends Undecuplet<A, B, C, D, E, F, G, H, I, J, K> {

    /**
     * Variable to store twelfth value
     */
    @Nullable
    protected final L twelfth;


    /**
     * Creates a Duodecuplet from the given values.
     * @param first the first value of the Duodecuplet
     * @param second the second value of the Duodecuplet
     * @param third the third value of the Duodecuplet
     * @param fourth the fourth value of the Duodecuplet
     * @param fifth the fifth value of the Duodecuplet
     * @param sixth the sixth value of the Duodecuplet
     * @param seventh the seventh value of the Duodecuplet
     * @param eighth the eighth value of the Duodecuplet
     * @param ninth the ninth value of the Duodecuplet
     * @param tenth the tenth value of the Duodecuplet
     * @param eleventh the eleventh value of the Duodecuplet
     * @param twelfth the twelfth value of the Duodecuplet
     */
    public Duodecuplet(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                       final @Nullable D fourth, final @Nullable E fifth, final @Nullable F sixth,
                       final @Nullable G seventh, final @Nullable H eighth, final @Nullable I ninth,
                       final @Nullable J tenth, final @Nullable K eleventh, final @Nullable L twelfth) {
        super(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth, eleventh);
        this.twelfth = twelfth;
    }


    /**
     * Returns the twelfth value
     * @return the twelfth value, that can also be {@code null}
     */
    @Nullable
    public L getTwelfth() {
        return twelfth;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Duodecuplet and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Duodecuplet<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> that = (Duodecuplet<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?>) o;
        return Objects.equals(twelfth, that.twelfth);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), twelfth);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    public @NotNull String toString() {
        return(String.format("{%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s}", first, second, third, fourth, fifth,
                sixth, seventh, eighth, ninth, tenth, eleventh, twelfth));
    }


    /**
     * Returns an object array with a dimension of twelve containing all
     * elements of this duodecuplet
     * @return an object array with all attributes from this duodecuplet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third, fourth, fifth, sixth, seventh,
                eighth, ninth, tenth, eleventh, twelfth};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    public @NotNull List<Object> toList() {
        return ListUtil.appendToList(super.toList(), twelfth);
    }


    /**
     * Returns a thread-safe list containing all values of this duodecuplet in
     * the correct order.
     * @return a vector with all elements of this duodecuplet
     */
    @Override
    public @NotNull Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), twelfth);
    }
}
