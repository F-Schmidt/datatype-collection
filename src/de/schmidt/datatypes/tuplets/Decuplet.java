/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements an object type that can store ten corresponding
 * values within one object.
 * @param <A> the type of the first argument of the decuplet
 * @param <B> the type of the second argument of the decuplet
 * @param <C> the type of the third argument of the decuplet
 * @param <D> the type of the fourth argument of the decuplet
 * @param <E> the type of the fifth argument of the decuplet
 * @param <F> the type of the sixth argument of the decuplet
 * @param <G> the type of the seventh argument of the decuplet
 * @param <H> the type of the eighth argument of the decuplet
 * @param <I> the type of the ninth argument of the decuplet
 * @param <J> the type of the tenth argument of the decuplet
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Decuplet<A, B, C, D, E, F, G, H, I, J> extends Nonuplet<A, B, C, D, E, F, G, H, I> {

    /**
     * Variable to store tenth value
     */
    @Nullable
    protected final J tenth;


    /**
     * Creates a decuplet from the given values.
     * @param first the first value of the Decuplet
     * @param second the second value of the Decuplet
     * @param third the third value of the Decuplet
     * @param fourth the fourth value of the Decuplet
     * @param fifth the fifth value of the Decuplet
     * @param sixth the sixth value of the Decuplet
     * @param seventh the seventh value of the Decuplet
     * @param eighth the eighth value of the Decuplet
     * @param ninth the ninth value of the Decuplet
     * @param tenth the tenth value of the Decuplet
     */
    public Decuplet(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                    final @Nullable D fourth, final @Nullable E fifth, final @Nullable F sixth,
                    final @Nullable G seventh, final @Nullable H eighth, final @Nullable I ninth,
                    final @Nullable J tenth) {
        super(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth);
        this.tenth = tenth;
    }


    /**
     * Returns the tenth value
     * @return the tenth value, that can also be {@code null}
     */
    @Nullable
    public J getTenth() {
        return tenth;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Decuplet and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Decuplet<?, ?, ?, ?, ?, ?, ?, ?, ?, ?> decuplet = (Decuplet<?, ?, ?, ?, ?, ?, ?, ?, ?, ?>) o;
        return Objects.equals(tenth, decuplet.tenth);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), tenth);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    @NotNull
    public String toString() {
        return(String.format("{%s,%s,%s,%s,%s,%s,%s,%s,%s,%s}", first, second, third,
                fourth, fifth, sixth, seventh, eighth, ninth, tenth));
    }


    /**
     * Returns an object array with a dimension of ten containing all
     * elements of this decuplet
     * @return an object array with all attributes from this decuplet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third, fourth, fifth, sixth, seventh,
                eighth, ninth, tenth};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    public @NotNull List<Object> toList() {
        return ListUtil.appendToList(super.toList(), tenth);
    }


    /**
     * Returns a thread-safe list containing all values of this decuplet in
     * the correct order.
     * @return a vector with all elements of this decuplet
     */
    @Override
    public @NotNull Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), tenth);
    }


    /**
     * Creates an Undecuplet from the values of the given Decuplet and appends the given value k at the
     * last position. The original Decuplet remains untouched, all values are copied into the new
     * created Undecuplet.
     * @param decuplet an already existing Decuplet, that will be appended with the given value
     * @param k the value to be appended to the given decuplet
     * @param <A> the type of the first argument of the decuplet
     * @param <B> the type of the second argument of the decuplet
     * @param <C> the type of the third argument of the decuplet
     * @param <D> the type of the fourth argument of the decuplet
     * @param <E> the type of the fifth argument of the decuplet
     * @param <F> the type of the sixth argument of the decuplet
     * @param <G> the type of the seventh argument of the decuplet
     * @param <H> the type of the eighth argument of the decuplet
     * @param <I> the type of the ninth argument of the decuplet
     * @param <J> the type of the tenth argument of the decuplet
     * @param <K> the type of the value to be appended
     * @return a new Undecuplet containing all values of the given Decuplet and the appended value at the last
     * position
     */
    public static <A, B, C, D, E, F, G, H, I, J, K> Undecuplet<A, B, C, D, E, F, G, H, I, J, K> addValue(
            final Decuplet<A, B, C, D, E, F, G, H, I, J> decuplet, final @Nullable K k) {
        Objects.requireNonNull(decuplet, "The Decuplet<A,B,D,C,E,F,G,H,I,J> may not be null.");
        return new Undecuplet<>(decuplet.first, decuplet.second, decuplet.third, decuplet.fourth, decuplet.fifth,
                decuplet.sixth, decuplet.seventh, decuplet.eighth, decuplet.ninth, decuplet.tenth, k);
    }
}
