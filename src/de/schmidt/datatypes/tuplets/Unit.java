/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements a data type that can store exactly one generic value.
 * The use of this class is only recommended if additional elements are to be added
 * dynamically.
 * @param <A> the type of the value that is stored in the Unit
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Unit<A> {

    /**
     * Variable to store first attribute of a tuple
     */
    @Nullable
    protected final A first;


    /**
     * Constructs a unit object from a given value.
     * @param first the value to be stored within the Unit
     */
    public Unit(final @Nullable A first) {
        this.first = first;
    }


    /**
     * Returns the first object stored in the unit.
     * @return the first object, that can also be {@code null}
     */
    @Nullable
    public A getFirst() {
        return first;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type unit and the stored
     * first value is equal to this first value, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Unit<?> unit = (Unit<?>) o;
        return Objects.equals(first, unit.first);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(first);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    @NotNull
    public String toString() {
        return(String.format("{%s}", first));
    }


    /**
     * Creates an object array that has the exact capacity of one and only
     * contains the value stored in the unit.
     * @return an object array with all attributes from the unit
     */
    public Object @NotNull [] toArray() {
        return new Object[]{first};
    }


    /**
     * Creates a List containing all values from the unit. Internally an
     * ArrayList will be created, but to generalize, a List will be returned.
     * @return a object-list containing all attributes from the unit
     * @see ArrayList
     */
    @NotNull
    public List<Object> toList() {
        return ListUtil.appendToList(new ArrayList<>(1), first);
    }


    /**
     * Creates a vector object containing all attributes from the unit.
     * Use this method, if you need a thread-safe list-solution.
     * @return a vector-object containing all attributes from the unit
     * @see Vector
     */
    @NotNull
    public Vector<Object> toVector() {
        return ListUtil.appendToList(new Vector<>(1), first);
    }

    /**
     * Adds a value to an already existing Unit and returns a new Tuple containing all
     * values from the unit and the value to be added.
     * @param unit the unit, where the value will be added to
     * @param b the new value to be added to the unit
     * @param <A> the type of the value stored in the unit
     * @param <B> the type of the value that will be added
     * @return a Tuple containing the first value from the unit and the new value b as
     * second value
     */
    @NotNull
    @Contract("_, _ -> new")
    public static <A, B> Tuple<A, B> addValue(final Unit<A> unit, final B b) {
        Objects.requireNonNull(unit, "The Unit<A> may not be null.");
        return new Tuple<>(unit.first, b);
    }
}
