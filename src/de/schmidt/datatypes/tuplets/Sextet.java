/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements an object type that can store six corresponding
 * values within one object.
 * @param <A> the type of the first argument of the sextet
 * @param <B> the type of the second argument of the sextet
 * @param <C> the type of the third argument of the sextet
 * @param <D> the type of the fourth argument of the sextet
 * @param <E> the type of the fifth argument of the sextet
 * @param <F> the type of the sixth argument of the sextet
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Sextet<A, B, C, D, E, F> extends Quintet<A, B, C, D, E> {

    /**
     * Variable to store sixth value
     */
    protected final F sixth;


    /**
     * Constructs a Sextet from the given values.
     * @param first the first value of the Sextet
     * @param second the second value of the Sextet
     * @param third the third value of the Sextet
     * @param fourth the fourth value of the Sextet
     * @param fifth the fifth value of the Sextet
     * @param sixth the sixth value of the Sextet
     */
    public Sextet(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                  final @Nullable D fourth, final @Nullable E fifth, final @Nullable F sixth) {
        super(first, second, third, fourth, fifth);
        this.sixth = sixth;
    }


    /**
     * Returns the sixth value
     * @return the sixth value, that can also be {@code null}
     */
    @Nullable
    public F getSixth() {
        return sixth;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Sextet and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Sextet<?, ?, ?, ?, ?, ?> sextet = (Sextet<?, ?, ?, ?, ?, ?>) o;
        return Objects.equals(sixth, sextet.sixth);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sixth);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    public @NotNull String toString() {
        return(String.format("{%s,%s,%s,%s,%s,%s}", first, second, third, fourth, fifth, sixth));
    }


    /**
     * Returns an object array with a dimension of six containing all
     * elements of this sextet
     * @return an object array with all attributes from this sextet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{fifth, second, third, fourth, fifth, sixth};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    public @NotNull List<Object> toList() {
        return ListUtil.appendToList(super.toList(), sixth);
    }


    /**
     * Returns a thread-safe list containing all values of this sextet in
     * the correct order.
     * @return a vector with all elements of this sextet
     */
    @Override
    public @NotNull Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), sixth);
    }


    /**
     * Creates a Septet from the values of the given Sextet and appends the given value g at the
     * last position. The original Sextet remains untouched, all values are copied into the new
     * created Septet.
     * @param sextet an already existing Sextet, that will be appended with the given value
     * @param g the value to be appended to the given sextet
     * @param <A> the type of the first argument of the sextet
     * @param <B> the type of the second argument of the sextet
     * @param <C> the type of the third argument of the sextet
     * @param <D> the type of the fourth argument of the sextet
     * @param <E> the type of the fifth argument of the sextet
     * @param <F> the type of the sixth argument of the sextet
     * @param <G> the type of the value to be appended
     * @return a new Septet containing all values of the given Sextet and the appended value at the last
     * position
     */
    @NotNull
    public static <A, B, C, D, E, F, G> Septet<A, B, C, D, E, F, G> addValue(final Sextet<A, B, C, D, E, F> sextet,
                                                                             final @Nullable G g) {
        Objects.requireNonNull(sextet, "The given Sextet<A,B,C,D,E,F> may not be null.");
        return new Septet<>(sextet.first, sextet.second, sextet.third, sextet.fourth,
                sextet.fifth, sextet.sixth, g);
    }
}
