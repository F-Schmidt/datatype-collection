/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements an object type that can store eleven corresponding
 * values within one object.
 * @param <A> the type of the first argument of the undecuplet
 * @param <B> the type of the second argument of the undecuplet
 * @param <C> the type of the third argument of the undecuplet
 * @param <D> the type of the fourth argument of the undecuplet
 * @param <E> the type of the fifth argument of the undecuplet
 * @param <F> the type of the sixth argument of the undecuplet
 * @param <G> the type of the seventh argument of the undecuplet
 * @param <H> the type of the eighth argument of the undecuplet
 * @param <I> the type of the ninth argument of the undecuplet
 * @param <J> the type of the tenth argument of the undecuplet
 * @param <K> the type of the eleventh argument of the undecuplet
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Undecuplet<A, B, C, D, E, F, G, H, I, J, K> extends Decuplet<A, B, C, D, E, F, G, H, I, J> {

    /**
     * Variable to store eleventh value
     */
    @Nullable
    protected final K eleventh;


    /**
     * Creates an Undecuplet from the given values.
     * @param first the first value of the Undecuplet
     * @param second the second value of the Undecuplet
     * @param third the third value of the Undecuplet
     * @param fourth the fourth value of the Undecuplet
     * @param fifth the fifth value of the Undecuplet
     * @param sixth the sixth value of the Undecuplet
     * @param seventh the seventh value of the Undecuplet
     * @param eighth the eighth value of the Undecuplet
     * @param ninth the ninth value of the Undecuplet
     * @param tenth the tenth value of the Undecuplet
     * @param eleventh the eleventh value of the Undecuplet
     */
    public Undecuplet(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                      final @Nullable D fourth, final @Nullable E fifth, final @Nullable F sixth,
                      final @Nullable G seventh, final @Nullable H eighth, final @Nullable I ninth,
                      final @Nullable J tenth, final @Nullable K eleventh) {
        super(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth);
        this.eleventh = eleventh;
    }


    /**
     * Returns the eleventh value
     * @return the eleventh value, that can also be {@code null}
     */
    @Nullable
    public K getEleventh() {
        return eleventh;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Undecuplet and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Undecuplet<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> that = (Undecuplet<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?>) o;
        return Objects.equals(eleventh, that.eleventh);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), eleventh);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    public @NotNull String toString() {
        return(String.format("{%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s}", first, second, third, fourth, fifth,
                sixth, seventh, eighth, ninth, tenth, eleventh));
    }


    /**
     * Returns an object array with a dimension of eleven containing all
     * elements of this undecuplet
     * @return an object array with all attributes from this undecuplet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third, fourth, fifth, sixth, seventh,
                eighth, ninth, tenth, eleventh};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    public @NotNull List<Object> toList() {
        return ListUtil.appendToList(super.toList(), eleventh);
    }


    /**
     * Returns a thread-safe list containing all values of this undecuplet in
     * the correct order.
     * @return a vector with all elements of this undecuplet
     */
    @Override
    public @NotNull Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), eleventh);
    }


    /**
     * Creates a Duodecuplet from the values of the given Undecuplet and appends the given value l at the
     * last position. The original Undecuplet remains untouched, all values are copied into the new
     * created Duodecuplet.
     * @param undecuplet an already existing Undecuplet, that will be appended with the given value
     * @param l the value to be appended to the given undecuplet
     * @param <A> the type of the first argument of the Undecuplet
     * @param <B> the type of the second argument of the Undecuplet
     * @param <C> the type of the third argument of the Undecuplet
     * @param <D> the type of the fourth argument of the Undecuplet
     * @param <E> the type of the fifth argument of the Undecuplet
     * @param <F> the type of the sixth argument of the Undecuplet
     * @param <G> the type of the seventh argument of the Undecuplet
     * @param <H> the type of the eighth argument of the Undecuplet
     * @param <I> the type of the ninth argument of the Undecuplet
     * @param <J> the type of the tenth argument of the Undecuplet
     * @param <K> the type of the eleventh argument of the Undecuplet
     * @param <L> the type of the value to be appended
     * @return a new Duodecuplet containing all values of the given Undecuplet and the appended value at the last
     * position
     */
    @NotNull
    public static <A, B, C, D, E, F, G, H, I, J, K, L> Duodecuplet<A, B, C, D, E, F, G, H, I, J, K, L>
        addValue(final Undecuplet<A, B, C, D, E, F, G, H, I, J, K> undecuplet, final @Nullable L l) {
        Objects.requireNonNull(undecuplet, "The Undecuplet<A,B,C,D,E,F,G,H,I,J,K,L> may not be null.");
        return new Duodecuplet<>(undecuplet.first, undecuplet.second, undecuplet.third, undecuplet.fourth,
                undecuplet.fifth, undecuplet.sixth, undecuplet.seventh, undecuplet.eighth, undecuplet.ninth,
                undecuplet.tenth, undecuplet.eleventh, l);
    }
}
