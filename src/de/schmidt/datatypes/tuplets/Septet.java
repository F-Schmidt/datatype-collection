/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements an object type that can store seven corresponding
 * values within one object.
 * @param <A> the type of the first argument of the septet
 * @param <B> the type of the second argument of the septet
 * @param <C> the type of the third argument of the septet
 * @param <D> the type of the fourth argument of the septet
 * @param <E> the type of the fifth argument of the septet
 * @param <F> the type of the sixth argument of the septet
 * @param <G> the type of the seventh argument of the septet
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Septet<A, B, C, D, E, F, G> extends Sextet<A, B, C, D, E, F> {

    /**
     * Variable to store seventh value
     */
    @Nullable
    protected final G seventh;


    /**
     * Constructs a Septet from the given values.
     * @param first the first value of the Septet
     * @param second the second value of the Septet
     * @param third the third value of the Septet
     * @param fourth the fourth value of the Septet
     * @param fifth the fifth value of the Septet
     * @param sixth the sixth value of the Septet
     * @param seventh the seventh value of the Septet
     */
    public Septet(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                  final @Nullable D fourth, final @Nullable E fifth, final @Nullable F sixth,
                  final @Nullable G seventh) {
        super(first, second, third, fourth, fifth, sixth);
        this.seventh = seventh;
    }


    /**
     * Returns the seventh value
     * @return the seventh value, that can also be {@code null}
     */
    @Nullable
    public G getSeventh() {
        return seventh;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Septet and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Septet<?, ?, ?, ?, ?, ?, ?> septet = (Septet<?, ?, ?, ?, ?, ?, ?>) o;
        return Objects.equals(seventh, septet.seventh);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), seventh);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    public @NotNull String toString() {
        return(String.format("{%s,%s,%s,%s,%s,%s,%s}", first, second, third, fourth, fifth, sixth, seventh));
    }


    /**
     * Returns an object array with a dimension of seven containing all
     * elements of this septet
     * @return an object array with all attributes from this septet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third, fourth, fifth, sixth, seventh};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    public @NotNull List<Object> toList() {
        return ListUtil.appendToList(super.toList(), seventh);
    }


    /**
     * Returns a thread-safe list containing all values of this septet in
     * the correct order.
     * @return a vector with all elements of this septet
     */
    @Override
    public @NotNull Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), seventh);
    }


    /**
     * Creates an Octet from the values of the given Septet and appends the given value h at the
     * last position. The original Septet remains untouched, all values are copied into the new
     * created Octet.
     * @param septet an already existing Septet, that will be appended with the given value
     * @param h the value to be appended to the given septet
     * @param <A> the type of the first argument of the septet
     * @param <B> the type of the second argument of the septet
     * @param <C> the type of the third argument of the septet
     * @param <D> the type of the fourth argument of the septet
     * @param <E> the type of the fifth argument of the septet
     * @param <F> the type of the sixth argument of the septet
     * @param <G> the type of the seventh argument of the septet
     * @param <H> the type of the value to be appended
     * @return a new Octet containing all values of the given Septet and the appended value at the last
     * position
     */
    public static <A, B, C, D, E, F, G, H> Octet<A, B, C, D, E, F, G, H> addValue(
            final Septet<A, B, C, D, E, F, G> septet, final @Nullable H h) {
        Objects.requireNonNull(septet, "The Septet<A,B,C,D,E,F,G> may not be null.");
        return new Octet<>(septet.first, septet.second, septet.third, septet.fourth, septet.fifth, septet.sixth,
                septet.seventh, h);
    }
}
