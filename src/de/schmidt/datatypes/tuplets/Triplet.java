/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements a triplet which is used to store three correlating values in one object.
 * Any value can be accessed directly via the respective getter.
 * @param <A> the type of the first value to be stored
 * @param <B> the type of the second value to be stored
 * @param <C> the type of the third value to be stored
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Triplet<A, B, C> extends Tuple<A, B> {

    /**
     * Variable to store third value
     */
    @Nullable
    protected final C third;


    /**
     * Constructs a triplet from given values.
     * @param first the first value to store
     * @param second the second value to store
     * @param third the third value to store
     */
    public Triplet(final @Nullable A first, final @Nullable B second, final @Nullable C third) {
        super(first, second);
        this.third = third;
    }


    /**
     * Returns the third value.
     * @return the third value, that can also be {@code null}
     */
    @Nullable
    public C getThird() {
        return third;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Triplet and the stored
     * first, second and third values are equal with these values, {@code false}
     * otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Triplet<?, ?, ?> triplet = (Triplet<?, ?, ?>) o;
        return Objects.equals(third, triplet.third);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), third);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    @NotNull
    public String toString() {
        return(String.format("{%s,%s,%s}", first, second, third));
    }


    /**
     * Returns an object array with a dimension of three containing all
     * elements of this triplet.
     * @return an object array with all attributes from this triplet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third};
    }


    /**
     * Returns a list containing all elements of the triplet.
     * @return a list with all elements from this triplet
     */
    @Override
    @NotNull
    public List<Object> toList() {
        return ListUtil.appendToList(super.toList(), third);
    }


    /**
     * Returns a thread-safe list containing all values of this triplet
     * in the correct order.
     * @return a vector with both elements from this triplet
     */
    @Override
    @NotNull
    public Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), third);
    }


    /**
     * Creates a Quadruple from this first, second and third value and appends the given value d at the
     * last position. The original Triplet remains untouched, all values are copied into the new
     * created quadruple.
     * @param triplet the original triplet that will be appended with d
     * @param d the value to append to the triplet
     * @param <A> the type of the first value of the Triplet
     * @param <B> the type of the second value of the Triplet
     * @param <C> the type of the third value of the Triplet
     * @param <D> the type of the parameter to be appended to the Triplet
     * @return a new Quadruple containing this.first, this.second, this.third and d
     */
    @NotNull
    @Contract("_, _ -> new")
    public static <A, B, C, D> Quadruple<A, B, C, D> addValue(final Triplet<A, B, C> triplet, final @Nullable D d) {
        Objects.requireNonNull(triplet, "The Triplet<A,B,C> may not be null.");
        return new Quadruple<>(triplet.first, triplet.second, triplet.third, d);
    }
}
