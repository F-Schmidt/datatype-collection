/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements a pair of values. Each element can be accessed directly, but
 * cannot be changed. In addition, useful methods for exporting to other data types
 * are implemented.
 * @param <A> the type of the first argument of this tuple
 * @param <B> the type of the second argument of this tuple
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Tuple<A, B> extends Unit<A> {

    /**
     * Variable to store second value
     */
    @Nullable
    protected final B second;


    /**
     * Constructs a Tuple from the given values by also
     * calling the super constructor {@link Unit#Unit(Object)}
     * @param first the first value to store
     * @param second the second value to store
     */
    public Tuple(final @Nullable A first, final @Nullable B second) {
        super(first);
        this.second = second;
    }


    /**
     * Returns the second object.
     * @return the second object, that can also be {@code null}
     */
    @Nullable
    public B getSecond() {
        return second;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Tuple and the stored
     * first and second values are equal with these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Tuple<?, ?> tuple = (Tuple<?, ?>) o;
        return Objects.equals(second, tuple.second);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), second);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    @NotNull
    public String toString() {
        return(String.format("{%s,%s}", first, second));
    }


    /**
     * Returns an object array with a dimension of two containing both elements
     * of this tuple
     * @return an object array with both attributes from this tuple
     */
    public Object @NotNull [] toArray() {
        return new Object[]{first, second};
    }


    /**
     * Returns a list containing both elements of the tuple.
     * @return a list with both elements from this tuple
     */
    @NotNull
    public List<Object> toList() {
        return ListUtil.appendToList(super.toList(), second);
    }


    /**
     * Returns a thread-safe list containing all values of this tuple in
     * the correct order.
     * @return a vector with both elements from this tuple
     */
    @NotNull
    public Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), second);
    }


    /**
     * Returns a mutable map entry with the first value as key value and the
     * second value of this tuple as entry value.
     * @return a mutable map entry with this.first as key value
     * @see java.util.AbstractMap.SimpleEntry#SimpleEntry(Object, Object)
     */
    @NotNull
    public Map.Entry<A, B> toMapEntry() {
        return new AbstractMap.SimpleEntry<>(first, second);
    }


    /**
     * Returns an immutable map entry with the first value as key value and the
     * second value of this tuple as entry value.
     * @return an immutable map entry with this.first as key value
     * @see java.util.AbstractMap.SimpleImmutableEntry#SimpleImmutableEntry(Object, Object)
     */
    @NotNull
    public Map.Entry<A, B> toImmutableMapEntry() {
        return new AbstractMap.SimpleImmutableEntry<>(first, second);
    }


    /**
     * Creates a Triplet from this first and second value and appends the given value c at the
     * last position. The original Tuple remains untouched, all values are copied into the new
     * created triplet.
     * @param tuple an already existing tuple that will be appended by another value
     * @param c the value to be appended to the given tuple
     * @param <A> the type of the first argument of the tuple
     * @param <B> the type of the second argument of the tuple
     * @param <C> the type of argument c
     * @return a new Triplet containing this.first, this.second and c
     */
    @NotNull
    @Contract("_, _ -> new")
    public static <A, B, C> Triplet<A, B, C> addValue(final Tuple<A, B> tuple, final C c) {
        Objects.requireNonNull(tuple, "The Tuple<A,B> may not be null.");
        return new Triplet<>(tuple.first, tuple.second, c);
    }
}
