/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements an object type that can store nine corresponding
 * values within one object.
 * @param <A> the type of the first argument of the nonuplet
 * @param <B> the type of the second argument of the nonuplet
 * @param <C> the type of the third argument of the nonuplet
 * @param <D> the type of the fourth argument of the nonuplet
 * @param <E> the type of the fifth argument of the nonuplet
 * @param <F> the type of the sixth argument of the nonuplet
 * @param <G> the type of the seventh argument of the nonuplet
 * @param <H> the type of the eight argument of the nonuplet
 * @param <I> the type of the ninth argument of the nonuplet
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Nonuplet<A, B, C, D, E, F, G, H, I> extends Octet<A, B, C, D, E, F, G, H> {

    /**
     * Variable to store ninth value
     */
    @Nullable
    protected final I ninth;


    /**
     * Constructs a Nonuplet from the given values.
     * @param first the first value of the Nonuplet
     * @param second the second value of the Nonuplet
     * @param third the third value of the Nonuplet
     * @param fourth the fourth value of the Nonuplet
     * @param fifth the fifth value of the Nonuplet
     * @param sixth the sixth value of the Nonuplet
     * @param seventh the seventh value of the Nonuplet
     * @param eighth the eighth value of the Nonuplet
     * @param ninth the ninth value of the Nonuplet
     */
    public Nonuplet(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                    final @Nullable D fourth, final @Nullable E fifth, final @Nullable F sixth,
                    final @Nullable G seventh, final @Nullable H eighth, final @Nullable I ninth) {
        super(first, second, third, fourth, fifth, sixth, seventh, eighth);
        this.ninth = ninth;
    }


    /**
     * Returns the ninth value
     * @return the ninth value, that can also be {@code null}
     */
    @Nullable
    public I getNinth() {
        return ninth;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Nonuplet and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Nonuplet<?, ?, ?, ?, ?, ?, ?, ?, ?> nonuplet = (Nonuplet<?, ?, ?, ?, ?, ?, ?, ?, ?>) o;
        return Objects.equals(ninth, nonuplet.ninth);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), ninth);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    public @NotNull String toString() {
        return(String.format("{%s,%s,%s,%s,%s,%s,%s,%s,%s}", first, second, third, fourth, fifth,
                sixth, seventh, eighth, ninth));
    }


    /**
     * Returns an object array with a dimension of nine containing all
     * elements of this nonuplet
     * @return an object array with all attributes from this nonuplet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third, fourth, fifth, sixth, seventh, eighth, ninth};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    public @NotNull List<Object> toList() {
        return ListUtil.appendToList(super.toList(), ninth);
    }


    /**
     * Returns a thread-safe list containing all values of this nonuplet in
     * the correct order.
     * @return a vector with all elements of this nonuplet
     */
    @Override
    public @NotNull Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), ninth);
    }


    /**
     * Creates a Decuplet from the values of the given Nonuplet and appends the given value j at the
     * last position. The original Nonuplet remains untouched, all values are copied into the new
     * created Decuplet.
     * @param nonuplet an already existing Nonuplet, that will be appended with the given value
     * @param j the value to be appended to the given nonuplet
     * @param <A> the type of the first argument of the nonuplet
     * @param <B> the type of the second argument of the nonuplet
     * @param <C> the type of the third argument of the nonuplet
     * @param <D> the type of the fourth argument of the nonuplet
     * @param <E> the type of the fifth argument of the nonuplet
     * @param <F> the type of the sixth argument of the nonuplet
     * @param <G> the type of the seventh argument of the nonuplet
     * @param <H> the type of the eighth argument of the nonuplet
     * @param <I> the type of the ninth argument of the nonuplet
     * @param <J> the type of the value to be appended
     * @return a new Decuplet containing all values of the given Nonuplet and the appended value at the last
     * position
     */
    public static <A, B, C, D, E, F, G, H, I, J> Decuplet<A, B, C, D, E, F, G, H, I, J> addValue(
            final Nonuplet<A, B, C, D, E, F, G, H, I> nonuplet, final @Nullable J j) {
        Objects.requireNonNull(nonuplet, "The Nonuplet<A,B,C,D,E,F,G,H,I> may not be null.");
        return new Decuplet<>(nonuplet.first, nonuplet.second, nonuplet.third, nonuplet.fourth, nonuplet.fifth,
                nonuplet.sixth, nonuplet.seventh, nonuplet.eighth, nonuplet.ninth, j);
    }
}
