/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements a quadruple of values. Each element can be accessed directly,
 * but cannot be changed.
 * @param <A> the type of the first element of this Quadruple
 * @param <B> the type of the second element of this Quadruple
 * @param <C> the type of the third element of this Quadruple
 * @param <D> the type of the fourth element of this Quadruple
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Quadruple<A, B, C, D> extends Triplet<A, B, C> {

    /**
     * Variable to store fourth value
     */
    @Nullable
    protected final D fourth;


    /**
     * Creates a Quadruple from the four given values.
     * @param first the first value of the quadruple
     * @param second the second value of the quadruple
     * @param third the third value of the quadruple
     * @param fourth the fourth value of the quadruple
     */
    public Quadruple(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                     final @Nullable D fourth) {
        super(first, second, third);
        this.fourth = fourth;
    }


    /**
     * Returns the fourth value.
     * @return the fourth value, that can also be {@code null}
     */
    @Nullable
    public D getFourth() {
        return fourth;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Quadruple and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Quadruple<?, ?, ?, ?> quadruple = (Quadruple<?, ?, ?, ?>) o;
        return Objects.equals(fourth, quadruple.fourth);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), fourth);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    @NotNull
    public String toString() {
        return(String.format("{%s,%s,%s,%s}", first, second, third, fourth));
    }


    /**
     * Returns an object array with a dimension of four containing all
     * elements of this quadruple
     * @return an object array with all attributes from this quadruple
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third, fourth};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    public @NotNull List<Object> toList() {
        return ListUtil.appendToList(super.toList(), fourth);
    }


    /**
     * Returns a thread-safe list containing all values of this quadruple in
     * the correct order.
     * @return a vector with all elements of this quadruple
     */
    @Override
    @NotNull
    public Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), fourth);
    }


    /**
     * Creates a Quintet from the values of the given Quadruple and appends the given value c at the
     * last position. The original Quadruple remains untouched, all values are copied into the new
     * created Quintet.
     * @param quadruple an already existing Quadruple, that will be appended with the given value
     * @param e the value to be appended to the given quadruple
     * @param <A> the type of the first argument of the quadruple
     * @param <B> the type of the second argument of the quadruple
     * @param <C> the type of the third argument of the quadruple
     * @param <D> the type of the fourth argument of the quadruple
     * @param <E> the type of the value to be appended
     * @return a new Quintet containing all values of the given Quadruple and the appended value at the last
     * position
     */
    @NotNull
    public static <A, B, C, D, E> Quintet<A, B, C, D, E> addValue(final Quadruple<A, B, C, D> quadruple,
                                                                  final @Nullable E e) {
        Objects.requireNonNull(quadruple, "The Quadruple<A,B,C,D> may not be null.");
        return new Quintet<>(quadruple.first, quadruple.second, quadruple.third, quadruple.fourth, e);
    }
}
