/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements a quintet of values. Each element can be accessed directly,
 * but cannot be changed.
 * @param <A> the type of the first element of this Quadruple
 * @param <B> the type of the second element of this Quadruple
 * @param <C> the type of the third element of this Quadruple
 * @param <D> the type of the fourth element of this Quadruple
 * @param <E> the type of the fifth element of this Quadruple
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Quintet<A, B, C, D, E> extends Quadruple<A, B, C, D> {

    /**
     * Variable to store fifth value
     */
    @Nullable
    protected final E fifth;


    /**
     * Constructs a Quintet from the given values.
     * @param first the first value of the Quintet
     * @param second the second value of the Quintet
     * @param third the third value of the Quintet
     * @param fourth the fourth value of the Quintet
     * @param fifth the fifth value of the Quintet
     */
    public Quintet(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                   final @Nullable D fourth, final @Nullable E fifth) {
        super(first, second, third, fourth);
        this.fifth = fifth;
    }


    /**
     * Returns the fifth value.
     * @return the fifth value, that can also be {@code null}
     */
    @Nullable
    public E getFifth() {
        return fifth;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Quintet and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Quintet<?, ?, ?, ?, ?> quintet = (Quintet<?, ?, ?, ?, ?>) o;
        return Objects.equals(fifth, quintet.fifth);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), fifth);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    public @NotNull String toString() {
        return(String.format("{%s,%s,%s,%s,%s}", first, second, third, fourth, fifth));
    }


    /**
     * Returns an object array with a dimension of five containing all
     * elements of this quintet
     * @return an object array with all attributes from this quintet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third, fourth, fifth};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    public @NotNull List<Object> toList() {
        return ListUtil.appendToList(super.toList(), fifth);
    }


    /**
     * Returns a thread-safe list containing all values of this quintet in
     * the correct order.
     * @return a vector with all elements of this quintet
     */
    @Override
    public @NotNull Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), fifth);
    }


    /**
     * Creates a Sextet from the values of the given Quintet and appends the given value c at the
     * last position. The original Quintet remains untouched, all values are copied into the new
     * created Sextet.
     * @param quintet an already existing Quintet, that will be appended with the given value
     * @param f the value to be appended to the given quintet
     * @param <A> the type of the first argument of the quadruple
     * @param <B> the type of the second argument of the quadruple
     * @param <C> the type of the third argument of the quadruple
     * @param <D> the type of the fourth argument of the quadruple
     * @param <E> the type of the fifth argument of the quadruple
     * @param <F> the type of the value to be appended
     * @return a new Sextet containing all values of the given Quintet and the appended value at the last
     * position
     */
    @NotNull
    @Contract("_, _ -> new")
    public static <A, B, C, D, E, F> Sextet<A, B, C, D, E, F> addValue(final Quintet<A, B, C, D, E> quintet,
                                                                                final @Nullable F f) {
        Objects.requireNonNull(quintet, "The Quintet<A,B,C,D,E> may not be null.");
        return new Sextet<>(quintet.first, quintet.second, quintet.third, quintet.fourth, quintet.fifth, f);
    }
}
