/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.tuplets;

import de.schmidt.datatypes.util.ListUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class implements an object type that can store eight corresponding
 * values within one object.
 * @param <A> the type of the first argument of the octet
 * @param <B> the type of the second argument of the octet
 * @param <C> the type of the third argument of the octet
 * @param <D> the type of the fourth argument of the octet
 * @param <E> the type of the fifth argument of the octet
 * @param <F> the type of the sixth argument of the octet
 * @param <G> the type of the seventh argument of the octet
 * @param <H> the type of the eighth argument of the octet
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Octet<A, B, C, D, E, F, G, H> extends Septet<A, B, C, D, E, F, G> {

    /**
     * Variable to store eighth value
     */
    @Nullable
    protected final H eighth;


    /**
     * Constructs an Octet from the given values.
     * @param first the first value of the Octet
     * @param second the second value of the Octet
     * @param third the third value of the Octet
     * @param fourth the fourth value of the Octet
     * @param fifth the fifth value of the Octet
     * @param sixth the sixth value of the Octet
     * @param seventh the seventh value of the Octet
     * @param eighth the eighth value of the Octet
     */
    public Octet(final @Nullable A first, final @Nullable B second, final @Nullable C third,
                 final @Nullable D fourth, final @Nullable E fifth, final @Nullable F sixth,
                 final @Nullable G seventh, final @Nullable H eighth) {
        super(first, second, third, fourth, fifth, sixth, seventh);
        this.eighth = eighth;
    }


    /**
     * Returns the eighth value
     * @return the eighth value, that can also be {@code null}
     */
    @Nullable
    public H getEighth() {
        return eighth;
    }


    /**
     * Compares an object to this one.
     * @param o the object to compare this to
     * @return {@code true}, if the given object is of type Octet and the stored
     * values are equal to these values, {@code false} otherwise
     */
    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Octet<?, ?, ?, ?, ?, ?, ?, ?> octet = (Octet<?, ?, ?, ?, ?, ?, ?, ?>) o;
        return Objects.equals(eighth, octet.eighth);
    }


    /**
     * Computes a hashCode for this object.
     * @return a hashcode for this objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), eighth);
    }


    /**
     * Creates a string representation for this object,
     * can be used for debugging and / or monitoring.
     * @return a string containing all attributes from this object
     */
    @Override
    @NotNull
    public String toString() {
        return(String.format("{%s,%s,%s,%s,%s,%s,%s,%s}", first, second, third, fourth,
                fifth, sixth, seventh, eighth));
    }


    /**
     * Returns an object array with a dimension of eight containing all
     * elements of this octet
     * @return an object array with all attributes from this octet
     */
    @Override
    public Object @NotNull [] toArray() {
        return new Object[]{first, second, third, fourth, fifth, sixth, seventh, eighth};
    }


    /**
     * Returns a list containing all elements of this object.
     * @return a list with all elements of this object
     */
    @Override
    @NotNull
    public List<Object> toList() {
        return ListUtil.appendToList(super.toList(), eighth);
    }


    /**
     * Returns a thread-safe list containing all values of this octet in
     * the correct order.
     * @return a vector with all elements of this octet
     */
    @Override
    @NotNull
    public Vector<Object> toVector() {
        return ListUtil.appendToList(super.toVector(), eighth);
    }


    /**
     * Creates a Nonuplet from the values of the given Octet and appends the given value i at the
     * last position. The original Octet remains untouched, all values are copied into the new
     * created Nonuplet.
     * @param octet an already existing Octet, that will be appended with the given value
     * @param i the value to be appended to the given octet
     * @param <A> the type of the first argument of the octet
     * @param <B> the type of the second argument of the octet
     * @param <C> the type of the third argument of the octet
     * @param <D> the type of the fourth argument of the octet
     * @param <E> the type of the fifth argument of the octet
     * @param <F> the type of the sixth argument of the octet
     * @param <G> the type of the seventh argument of the octet
     * @param <H> the type of the eight argument of the octet
     * @param <I> the type of the value to be appended
     * @return a new Nonuplet containing all values of the given Octet and the appended value at the last
     * position
     */
    @NotNull
    public static <A, B, C, D, E, F, G, H, I> Nonuplet<A, B, C, D, E, F, G, H, I> addValue(
            final Octet<A, B, C, D, E, F, G, H> octet, final @Nullable I i) {
        Objects.requireNonNull(octet, "The Octet<A,B,C,D,E,F,G,H> may not be null.");
        return new Nonuplet<>(octet.first, octet.second, octet.third, octet.fourth, octet.fifth, octet.sixth,
                octet.seventh, octet.eighth, i);
    }
}
