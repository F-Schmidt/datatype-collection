/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.mathematics.vectors;

import de.schmidt.datatypes.tuplets.Tuple;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class Vector2d extends Tuple<Double, Double> {

    public Vector2d(Double first, Double second) {
        super(first, second);
    }


    public double abs() {
        return(Math.sqrt(Math.pow(this.getFirst(), 2) + Math.pow(this.getSecond(), 2)));
    }

    //TODO Implementierung für 2D und 3D mit Vererbung und vielleicht einem Interface / abstrakte Klasse zur Berechnung
    // von bestimmten Methoden oder Vector-interface



    @Contract("_ -> new")
    public static @NotNull Vector2d fromTuple(Tuple<Number, Number> tuple) {
        Objects.requireNonNull(tuple);
        return new Vector2d(tuple.getFirst().doubleValue(), tuple.getSecond().doubleValue());
    }




}
