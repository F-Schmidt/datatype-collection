/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.mathematics;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("unused")
public final class Complex implements Cloneable, Comparable<Complex>, Serializable {

    /**
     * Field to store the real proportion of the complex number
     */
    private double real;


    /**
     * Field to store the imaginary proportion of the complex number
     */
    private double imag;


    /**
     * Constructs a complex number from the real and imaginary values.
     * @param real the real proportion of the complex number to be created
     * @param imag the imaginary proportion of the complex number to be created
     */
    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }


    /**
     * Returns the current real proportion of this complex number.
     * @return the real proportion
     */
    public double getReal() {
        return real;
    }


    /**
     * Returns the current imaginary proportion of this complex number.
     * @return the imaginary proportion
     */
    public double getImag() {
        return imag;
    }


    /**
     * Adds another Complex to this Complex number object. Therefore the real proportions and
     * the imaginary proportions are added with each other. If the complex to add to this one
     * is {@code null}, then this method will have no effect.
     * @param c another Complex-object to add to this one
     * @return a reference to this object
     */
    public Complex add(Complex c) {
        if (c != null) {
            this.real = this.real + c.getReal();
            this.imag = this.imag + c.getImag();
        }
        return this;
    }


    /**
     * Subtracts another Complex from this Complex number object. If the complex object to be
     * subtracted from this one is {@code null}, then this method will have no effect.
     * @param c another Complex-object to subtract from this one
     * @return a reference to this object
     */
    public Complex subtract(Complex c) {
        if (c != null) {
            this.real = this.real - c.getReal();
            this.imag = this.imag - c.getImag();
        }
        return this;
    }


    /**
     * Multiplies this complex with another complex object. If this complex was defined as
     * {@code z1 = a + bi} and the other complex was defined as {@code z2 = c + di}, then this
     * complex will be set to the result of {@code z1 * z2 = (ac - bd) + i(ad - cb)}.
     * @param c another Complex-object to multiply with this one
     * @return a reference to this object
     */
    public Complex multiply(Complex c) {
        if (c != null) {
            double real = this.real * c.getReal() - this.imag * c.getImag();
            double imag = this.real * c.getImag() + this.imag * c.getReal();
            this.real = real;
            this.imag = imag;
        }
        return this;
    }


    /**
     * Divides this complex number by the given complex number and stores the result value in this complex
     * number. If the given complex number is null, then this method has no effect. If the given complex
     * number is {@code null}, then this method will throw an exception.
     * @param c the complex value by which this will be divided
     * @return a reference to this object
     * @throws ArithmeticException if the complex value by which this will be divided has a real and imaginary
     * proportion of zero
     */
    public Complex divide(Complex c) throws ArithmeticException {
        if (c != null) {
            if (Double.compare(c.getReal(), 0D) == 0 &&
                    Double.compare(c.getImag(), 0D) == 0) {
                throw new ArithmeticException("This complex number may not be divided by zero-value.");
            }
            double denominator = Math.pow(c.getReal(), 2) + Math.pow(c.getImag(), 2);
            double real = (this.real * c.getReal() + this.imag * c.getImag()) / denominator;
            double imag = (this.imag * c.getReal() - this.real * c.getImag()) / denominator;
            this.real = real;
            this.imag = imag;
        }
        return this;
    }


    /**
     * Calculates the absolut of this complex number. Therefore the geometric vector length will
     * be calculated.
     * @return Returns the 'length' of the complex pointer.
     */
    public double abs() {
        return (Math.sqrt(Math.pow(real, 2) + Math.pow(imag, 2)));
    }


    /**
     * Inverts the sign of the complex part and thus ensures that the complex number is mirrored
     * on the x-axis. The result is the conjugate number and will be stored in this complex number.
     * @return a reference to this complex number
     */
    @Contract(value = " -> new", pure = true)
    public @NotNull Complex conjugate() {
        this.imag = (-1) * this.imag;
        return this;
    }


    /**
     * Calculates the angle of this complex value in relation to the x-axis.
     * Therefore {@link Math#atan2(double, double)} will be used
     * @return the angle between this complex number and the x-axis in radians
     * @see Math#atan2(double, double)
     */
    public double calculateAngle() {
        return (Math.atan2(this.imag, this.real));
    }


    /**
     * Compares this complex value with a given object.
     * @param o the object to compare this to
     * @return {@code true} if the given object is a {@link Complex} and real and imaginary
     * proportion are the same, {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Complex complex = (Complex) o;
        return(Double.compare(complex.real, real) == 0 &&
                Double.compare(complex.imag, imag) == 0);
    }


    /**
     * Computes a hashcode for this object.
     * @return the hashcode according to this object
     */
    @Override
    public int hashCode() {
        return Objects.hash(real, imag);
    }


    /**
     * Returns a string representation of format {@code a + bi}.
     * @return a string including all attributes of this object
     */
    @Override
    public String toString() {
        return String.format("%f + %fi", this.real, this.imag);
    }


    /**
     * Returns a string representation of format {@code a + bj}.
     * Please notice the difference, that the imaginary unit will be output with
     * a {@code j} instead of a {@code i}. If you prefer the mathematical
     * representation, then use {@link #toString()} instead.
     * @return a string including all attributes of this object
     */
    public String toEngineeringString() {
        return String.format("%f + %fj", this.real, this.imag);
    }


    /**
     * Creates a new Complex object with the same attributes as this one and returns
     * the reference to the new object.
     * @return a reference to the new Complex object copy
     */
    @Override
    public synchronized @NotNull Complex clone() {
        try {
            return (Complex) super.clone();
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, because this class is cloneable
            throw new InternalError(e);
        }
    }


    /**
     * Compares two complex numbers by comparing the amounts of both.
     * @param o the complex object to compare this to
     * @return an {@code int} less than zero, if this complex number has a smaller amount,
     * an {@code int} greater than zero, if this complex number has a greater amount than the given one.
     * This method will return zero, if both complex values lie on the same circle
     */
    @Override
    public int compareTo(@NotNull Complex o) {
        return Double.compare(this.abs() - o.abs(), 0);
    }
}
