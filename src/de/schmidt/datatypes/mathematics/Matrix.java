/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.mathematics;

import org.jetbrains.annotations.NotNull;

import java.util.Random;
import java.util.Vector;

public final class Matrix {

    private final Vector<Vector<Double>> values;

    public Matrix() {
        this.values = new Vector<>(); //TODO
    }

    public Matrix(int x, int y) {
        this.values = new Vector<>(x);
    }

    @NotNull
    public static Matrix generateRandom(int x, int y) {
        Random random = new Random();
        Matrix matrix = new Matrix();
        for(int i = 0; i < x; i++) {
            for(int j = 0; j < y; j++) {
                matrix.values.get(i).set(j, random.nextDouble());
            }
        }
        return matrix;
    }

    //TODO add, multiply, potenz, etc....vgl mit HM3, check ob diagbar, gib Eigenwerte, EVs aus etc
}
