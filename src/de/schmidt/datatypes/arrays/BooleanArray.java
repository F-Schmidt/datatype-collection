package de.schmidt.datatypes.arrays;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * This class offers a data type that can store and return boolean values easily. After extensive tests, it was found
 * that better memory efficiency and shorter write and read times for a number of up to 1000 values is almost
 * guaranteed compared to the primitive approach with boolean[].
 *
 * All Boolean values are saved as a single bit. The internal data consists of a byte array, which is manipulated
 * using bit operators. This class is deliberately minimally implemented in order to not unnecessarily provide
 * unused methods. If further methods are required in the course of a project, the use of utility classes is
 * recommended. Subclasses are not supported because this class is final and therefore can not be inherited.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
@SuppressWarnings("unused")
public final class BooleanArray implements Cloneable, Comparable<BooleanArray>, Serializable {

    /**
     * Internal variable to store the initial size.
     * If {@link #BooleanArray()} is called, then internally there will be a
     * byte array created with a capacity of one byte, which is equal to eight booleans.
     */
    private static final int INITIAL_BYTE_SIZE = 1;


    /**
     * The stored boolean values
     */
    @NotNull
    private byte[] data;


    /**
     * This variable marks the amount of boolean values stored and the next index to store the
     * next given boolean value
     */
    private int length;


    /**
     * Only used for relative reading. If you call {@link #get()}, then this variable gets
     * incremented internally.
     */
    private int readingCursor;


    /**
     * Creates a default BooleanArray with the internal capacity of {@link #INITIAL_BYTE_SIZE}.
     */
    public BooleanArray() {
        this.data = new byte[INITIAL_BYTE_SIZE];
        this.length = 0;
        this.readingCursor = 0;
    }

    /**
     * Creates a BooleanArray with a capacity of at least the given capacity.
     * @param capacity the amount of boolean values that can be stored. It is possible that the internal byte array
     *                 can store more booleans, if {@code capacity%8!=0}, but it is guaranteed, that the given amount
     *                 of booleans can always be stored.
     * @throws IllegalArgumentException if the capacity is less than zero
     */
    public BooleanArray(int capacity) throws IllegalArgumentException {
        if(capacity < 0) {
            throw new IllegalArgumentException("The capacity may not be less than zero.");
        }
        this.data = new byte[capacity%8==0 ? capacity/8 : capacity/8+1];
        this.length = 0;
        this.readingCursor = 0;
    }

    /**
     * Creates a BooleanArray from the given boolean data.
     * @param data the boolean values to be stored
     * @throws NullPointerException if data is null
     */
    public BooleanArray(boolean... data) throws NullPointerException {
        Objects.requireNonNull(data, "The given boolean values may not be null.");
        this.length = 0;
        this.readingCursor = 0;
        initDataArray(data);
    }

    /**
     * Internally used method to create a bytearray from a given BooleanArray
     * @param booleans an array containing boolean values to be added
     */
    private void initDataArray(@NotNull boolean[] booleans) {
        this.data = new byte[(booleans.length%8==0 ? booleans.length/8 : (booleans.length/8)+1)];
        for(boolean b : booleans) {
            this.append(b);
        }
    }

    /**
     * Appends a boolean to the next free space. If there is no capacity left, then this object will be expanded.
     * @param b the boolean value to store
     * @return a reference to this object to enable method chaining
     */
    public BooleanArray append(boolean b) {
        if(this.length >= capacity()) {
            this.resize();
        }
        return this.set(this.length++, b);
    }

    /**
     * Appends all given boolean values by calling {@link #append(boolean)} for each boolean value
     * @param bools the boolean values to append
     * @return a reference to this object to enable method chaining
     * @throws NullPointerException if bools are null
     */
    public BooleanArray appendAll(@NotNull boolean... bools) throws NullPointerException {
        for(boolean b : bools) {
            this.append(b);
        }
        return this;
    }

    /**
     * Sets a boolean value at the given index.
     * @param b the boolean value to store / set
     * @param index the index to save b to
     * @return a reference to this object to enable method chaining
     * @throws ArrayIndexOutOfBoundsException if index is less than zero or greater than {@link #capacity()}
     */
    public BooleanArray set(int index, boolean b) throws ArrayIndexOutOfBoundsException {
        int position = index/8;
        if(b) {
            this.data[position] |= 1 << (index % 8);
        } else {
            this.data[position] &= ~(1 << (index % 8));
        }
        return this;
    }

    /**
     * Returns the boolean value at the given index.
     * @param index the position where the boolean value is stored
     * @return the boolean stored at the position index
     * @throws ArrayIndexOutOfBoundsException if index is less than zero or greater than {@link #capacity()}
     */
    public boolean get(int index) throws ArrayIndexOutOfBoundsException {
        if(index < 0 || index > this.length) {
            throw new ArrayIndexOutOfBoundsException(String.format("Index [%d] must be in range of [0,%d)",
                    index, this.length));
        }
        return(((this.data[index/8] >> (index % 8)) & 1) == 1);
    }

    /**
     * Reads this object relative. If this method is called, then first the boolean at position 0 is returned.
     * Calling this method once again, the boolean at position 1 is returned and so on.
     * @return the boolean value stored at the relative position
     * @throws ArrayIndexOutOfBoundsException if the internal index is less than zero or greater than
     * {@link #capacity()}
     */
    public boolean get() throws ArrayIndexOutOfBoundsException {
        return this.get(this.readingCursor++);
    }

    /**
     * Toggles a boolean value at the given index using bitwise XOR-Operation.
     * @param index the index to toggle the boolean value at
     * @throws ArrayIndexOutOfBoundsException if index is less than zero or greater than {@link #capacity()}
     */
    public void toggle(int index) throws ArrayIndexOutOfBoundsException {
        this.data[index/8] ^= 1 << (index%8);
    }

    /**
     * Creates and returns a new BooleanArray with inverted bits, the original BooleanArray remains untouched.
     * @return a new BooleanArray with inverted bits.
     */
    @NotNull
    public BooleanArray invert() {
        BooleanArray newBooleanArray = this.clone();
        for(int i = 0; i < newBooleanArray.length; i++) {
            newBooleanArray.toggle(i);
        }
        return newBooleanArray;
    }

    /**
     * Internally used method to increase the data size, if the amount of boolean values to be stored exceeds
     * {@link #capacity()}.
     */
    private void resize() {
        byte[] newBytes = new byte[2*this.data.length];
        System.arraycopy(this.data, 0, newBytes, 0, this.length);
        this.data = newBytes;
    }


    /**
     * Internally used method to show the bit-structure stored in this object.
     * @param bytes the internally used byte array {@link #data}
     * @return a binary {@link String} representing the bitvalues of {@link #data}
     */
    @NotNull
    private static String dataToString(@NotNull byte[] bytes) {
        StringBuilder sb = new StringBuilder().append("[");
        String s;
        for(byte b : bytes) {
            s = String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
            sb.append(new StringBuilder(s).reverse());
            sb.append(", ");
        }
        return sb.replace(sb.length()-2, sb.length()-1, "]").toString();
    }

    /**
     * Returns the amount of boolean values stored.
     * @return the amount of stored booleans
     */
    public int size() {
        return length;
    }

    /**
     * Returns the number of boolean values that can be stored.
     * @return the number of boolean values, that can be stored within this object before calling {@link #resize()}.
     */
    public int capacity() {
        return Byte.SIZE*this.data.length;
    }

    /**
     * Returns a copy of the internally used byte array to store all boolean values.
     * @return a byte array including all boolean values
     */
    @NotNull
    public byte[] toBytes() {
        byte[] dataCopy = new byte[this.length%8 == 0 ? this.length/8 : this.length%8+1];
        System.arraycopy(this.data, 0, dataCopy, 0, this.length%8 == 0 ? this.length/8 : this.length%8+1);
        return dataCopy;
    }

    /**
     * Returns a new boolean array created from the internally stored bit values.
     * @return a boolean array representing the stored values
     */
    @NotNull
    public boolean[] array() {
        boolean[] b = new boolean[this.length];
        for(int i = 0; i < this.length; i++) {
            b[i] = this.get(i);
        }
        return b;
    }

    /**
     * Compares a given object to this one.
     * @param o the object to compare this to
     * @return {@code true} if all attributes are the same, {@code false} otherwise. If you only want to compare the
     * stored data, then do not use this method, use {@link #compareTo(BooleanArray)}.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BooleanArray that = (BooleanArray) o;
        return length == that.length &&
                readingCursor == that.readingCursor &&
                Arrays.equals(data, that.data);
    }

    /**
     * Computes the hashcode from this object
     * @return the computed hashcode from this object
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(length, readingCursor);
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    /**
     * Creates a string including all attributes from this object.
     * @return a string, that may be used for debugging only
     */
    @Override
    @NotNull
    public String toString() {
        return("BooleanArray{data=" + dataToString(data) + ", length=" + length +
                ", readingCursor=" + readingCursor + '}');
    }

    /**
     * Creates a new BooleanArray by copying all fields.
     * @return a new BooleanArray with the same attributes compared to this one
     */
    @Override
    @NotNull
    public synchronized BooleanArray clone() {
        try {
            BooleanArray booleanArray = (BooleanArray) super.clone();
            booleanArray.data = Arrays.copyOf(this.data, this.data.length);
            return booleanArray;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, because this class is cloneable
            throw new InternalError(e);
        }
    }


    /**
     * Compares this data to the data from the given BooleanArray. If both BooleanArrays have a different size, then
     * {@code this.size()-that.size} is returned. Otherwise all boolean values are compared to each other using
     * {@link Boolean#compare(boolean, boolean)}. If the bit-structure is the same, the 0 is returned.
     * If this bit-structure includes {@code false} at any position before that, then a negative value is returned,
     * otherwise a positive value will be returned.
     * @param that the BooleanArray to compare this to
     * @return zero, if both bit-structures are the same, a value less than zero, if this bit-structure contains
     * a {@link false} value at an earlier position than that, otherwise a positive value
     * @see Boolean#compare(boolean, boolean)
     */
    @Override
    public int compareTo(@NotNull BooleanArray that) {
        if(this.size() != that.size()) {
            return this.size() - that.size();
        }
        int comparison;
        for(int i = 0; i < this.size(); i++) {
            if((comparison = Boolean.compare(this.get(i), that.get(i))) != 0) {
                return comparison;
            }
        }
        return 0;
    }
}
