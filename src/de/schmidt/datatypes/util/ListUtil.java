/*
 * This JAVA-library extends the API by implementing some useful data types.
 * Datatypes (JAVA-library) Copyright (C) 2020  Falko Schmidt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package de.schmidt.datatypes.util;

import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * This class offers methods for use in combination with lists and should only
 * be used within this framework.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
public final class ListUtil {

    /**
     * Adds a value to the end of a given list. If the list is {@code null}, then {@code null} will be
     * returned without appending any value or causing any exception.
     * @param list the list where the value will be appended to
     * @param value the value to be appended to the list
     * @param <L> the type of the list (for example {@link java.util.ArrayList} or {@link java.util.Vector})
     * @param <E> the type of elements within the list
     * @param <T> the type of the value to be appended, which is a subtype of the element type in the list
     * @return a reference to the list with the appended value
     */
    public static <L extends List<E>, E, T extends E> L appendToList(final @Nullable L list, final @Nullable T value) {
        if(list != null) {
            list.add(value);
        }
        return list;
    }
}
