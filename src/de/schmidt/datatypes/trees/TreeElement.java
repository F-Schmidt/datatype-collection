package de.schmidt.datatypes.trees;

 final class TreeElement<E> {
    private E value;

    TreeElement(E value){
        this.value=value;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value){
        this.value=value;
    }



}
