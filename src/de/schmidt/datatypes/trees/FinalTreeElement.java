package de.schmidt.datatypes.trees;

final class FinalTreeElement<E>{
    private final E value;

    /* WAY better than public String getValue().. with a lot of parsing*/
    public E getValue() {
        return value;
    }




    FinalTreeElement(E value){
        this.value=value;
    }
}
