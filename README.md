# Datatype-Collection

This Java project implements various data types that are not provided by the Java API, but can nevertheless be used in various projects for better data structures and faster access.

Developed by Falko Schmidt