x = [1, 10, 100, 1000, 10000];%, 100000];
f = [3262.303, 8786.715, 9712.628, 16966.818, 18344.03];%, 19695.902]; %boolean[]
g = [518.793, 976.961, 5725.877, 28915.377, 41677.664];%, 219476.665]; %BooleanSet

semilogx(x, f, 'bs-', 'LineWidth', 2, 'MarkerSize', 10, 'Displayname', 'Generic boolean[ ]');
hold on;
semilogx(x, g, 'rs-', 'LineWidth', 2, 'MarkerSize', 10, 'Displayname', 'BooleanArray');
% Get handle to current axes.
ax = gca;

% Set background color (w=white, k=key[black])
ax.Color = 'w';

% Set grid and grid color. Alpha sets transparency, yTicks sets grid steps
grid on;
ax.GridColor = 'k';
ax.GridAlpha = 0.3;
%xticks(0 : 1 : 100);
%yticks(0 : 1 : 100);

% Set X axis
xlabel('Amount of booleans stored');
%darkGreen = [0, 0.6, 0];
ax.XColor = 'k';
ax.XAxis.FontSize = 20;

% Set Y axis
ylabel('Time to read all boolean entries in nanoseconds');
ax.YColor = 'k';
ax.YAxis.FontSize = 20;

% Set title
title('Readingefficiency of BooleanArray vs generic boolean array', 'FontSize', 24, 'Color', 'k', 'FontWeight', 'bold');

% Set legend
lgd = legend;
lgd.FontSize = 20;
lgd.Location = 'northwest';
lgd.Title.String = 'Reading comparison';

hold off;