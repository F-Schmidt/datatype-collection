x = 0:1:20;
f = 5+x;
g = [9,9,9,9,9,9,9,9,9,10,10,10,10,10,10,10,10,11,11,11,11];%8+x/8;

plot(x, f, 'bs-', 'LineWidth', 2, 'MarkerSize', 10, 'Displayname', 'Generic boolean[ ]');
hold on;
plot(x, g, 'rs-', 'LineWidth', 2, 'MarkerSize', 10, 'Displayname', 'BooleanSet');
% Get handle to current axes.
ax = gca;

% Set background color (w=white, k=key[black])
ax.Color = 'w';

% Set grid and grid color. Alpha sets transparency, yTicks sets grid steps
grid on;
ax.GridColor = 'k';
ax.GridAlpha = 0.3;
xticks(0 : 1 : 100);
yticks(0 : 1 : 100);

% Set X axis
xlabel('amount of booleans to be stored');
%darkGreen = [0, 0.6, 0];
ax.XColor = 'k';
ax.XAxis.FontSize = 20;

% Set Y axis
ylabel('total amount of bytes representing a bijective function of a boolean storage');
ax.YColor = 'k';
ax.YAxis.FontSize = 20;

% Set title
title('Storageefficiency of BooleanSet vs generic boolean array', 'FontSize', 24, 'Color', 'k', 'FontWeight', 'bold');

% Set legend
lgd = legend;
lgd.FontSize = 20;
lgd.Location = 'northwest';
lgd.Title.String = 'Storage comparison';

hold off;