x = [1, 10,100,1000,10000,100000];%, 1000000]; %1 to 1M
%Boolean[]
f = [4684.83, 5659.235, 9995.849, 30016.187, 35828.303, 93237.517];%, 1013598.27];
%BooleanSet
g = [731.055, 1551.291, 4810.125, 29028.913, 56987.411, 305687.018];%, 3246891.55];

%1146 vs 844 für 10 Einträge gewinnt BooleanSet...theoretisch

semilogx(x, f, 'bs-', 'LineWidth', 2, 'MarkerSize', 10, 'Displayname', 'Generic boolean[ ]');
hold on;
semilogx(x, g, 'rs-', 'LineWidth', 2, 'MarkerSize', 10, 'Displayname', 'BooleanArray');
% Get handle to current axes.
ax = gca;

% Set background color (w=white, k=key[black])
ax.Color = 'w';

% Set grid and grid color. Alpha sets transparency, yTicks sets grid steps
grid on;
ax.GridColor = 'k';
ax.GridAlpha = 0.3;
%xticks(0 : 1 : 100);
%yticks(0 : 1 : 100);

% Set X axis
xlabel('Amount of booleans to be written');
%darkGreen = [0, 0.6, 0];
ax.XColor = 'k';
ax.XAxis.FontSize = 20;

% Set Y axis
ylabel('Average writing time of 1.000 tests in nanoseconds');
ax.YColor = 'k';
ax.YAxis.FontSize = 20;

% Set title
title('Writing efficiency of BooleanArray vs generic boolean array', 'FontSize', 24, 'Color', 'k', 'FontWeight', 'bold');

% Set legend
lgd = legend;
lgd.FontSize = 20;
lgd.Location = 'northwest';
lgd.Title.String = 'Writing comparison';

hold off;